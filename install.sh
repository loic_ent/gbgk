#!/bin/bash
echo -e "\e[92m  Installation ...\e[0m"
cp -rf git-hooks ~/.git-hooks
git config --global core.hooksPath ~/.git-hooks
echo -e "\e[34m  Installation terminée\e[49m"
